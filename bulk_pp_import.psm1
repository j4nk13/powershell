﻿function Format-Addresses {
<#
.SYNOPSIS
Formats a file of email addresses for upload into proofpoint

.DESCRIPTION
Takes a list of email addresses, domains or ip addresses saved on separate lines
in a .txt file and formats them for bulk upload into a proofpoint block list

.PARAMETER filePath
The path of the list to format i.e C:\Users\someUser\Desktop\blocks.txt

.PARAMETER comment
A comment to add to proofpoint to explain the block

.EXAMPLE
Format-Addresses -filePath C:\Users\someUser\Desktop\blocks.txt -comment 'some comment'

#>
Param(
    [Parameter(Mandatory=$true)]
    [string]$filePath,
    [Parameter(Mandatory=$true)]
    [string]$comment
)

#import contents of your email file
$emails = Get-Content $filePath

#Add the info required by proofpoint for import and store in a new object
$output = foreach ($line in $emails){
    if ($line.StartsWith("@")) {
        ("`$from," + "match," + $line + "," + $comment)
    }
    elseif ($line -match '\b(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}\b') {
        ("`$ip," + "equal," + $line + "," + $comment)
    }
    else {
        ("`$from," + "equal," + $line + "," + $comment)
    }
}

#Write the object out to a new file
$output | Out-File $filePath -Encoding ascii

Write-Host "`nComplete! The file has been formatted.`n "
}
Export-ModuleMember -function Format-Addresses