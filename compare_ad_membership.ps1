#Script to compare active directory group membership of two accounts
#It will return any groups $BenchmarkAcc has membership in that $CompareAcc does not

param(
[Parameter(Mandatory=$true)]
[string] $BenchmarkAcc,
[Parameter(Mandatory=$true)]
[string] $CompareAcc

)

# Retrieves the group membership for both accounts
$BenchmarkMember = Get-AdUser -Filter {samaccountname -eq $BenchmarkAcc} -Property memberof | Select-Object memberof
$CompareMember = Get-AdUser -Filter {samaccountname -eq $CompareAcc } -Property memberof | Select-Object memberof

# Checks if accounts have group membership, if no group membership is found for either account script will exit
if ($BenchmarkMember -eq $null) {'Benchmark user not found';return}
if ($CompareMember -eq $null) {'Compare user not found';return}

# Checks for differences, if no differences are found script will prompt and exit
if (-not (Compare-Object $CompareMember.memberof $BenchmarkMember.memberof | 
Where-Object {$_.sideindicator -eq '=>'})) {write-host "`n`rNo difference between $BenchmarkAcc & $CompareAcc group membership found.";return}

# Performs the comparison and outputs the results to the console
write-host "`n`r$CompareAcc is not a member of:`n`r"
compare-object $CompareMember.memberof $BenchmarkMember.memberof | where-object {$_.sideindicator -eq '=>'} |
Select-Object -expand inputobject | foreach {write-host ([regex]::split($_,'^CN=|,OU=.+$'))[1]}