function Get-Pac {

<#
.SYNOPSIS
Download the specified production pac file

.DESCRIPTION
Download the specified production pac file

.PARAMETER fileName
Name of the pac file to be downloaded

.PARAMETER fileDest
Path of the download destination

.EXAMPLE
Get-Pac -fileName someName.pac -fileDest c:\some\file\destination
#>

Param(
    [Parameter(Mandatory=$true)]
    [string]$fileName,
    [Parameter(Mandatory=$true)]
    [string]$fileDest
)

$webAddress = Get-Content C:\windows\system32\WindowsPowerShell\v1.0\Modules\pac_file\pacURL.txt
Invoke-WebRequest -Uri "$webAddress/$fileName" -OutFile "$fileDest\$fileName"

}

function Publish-TestPac {

<#
.SYNOPSIS
Upload pac file to test directories

.DESCRIPTION
Upload pac file to the test directories on production servers
found in the pacserver.txt file

.PARAMETER filePath
Path of the pac file to be uploaded

.EXAMPLE
Publish-TestPac -filePath c:\file\path\someFile.pac 
#>

Param(
    [Parameter(Mandatory=$true)]
    [string]$filePath
)

$pacServers = Get-Content C:\windows\system32\WindowsPowerShell\v1.0\Modules\pac_file\pacservers.txt

foreach($_ in $pacServers){
    Try{
        Copy-Item -Path $filePath -Destination $_\testpac\
        $copyMsg = "Upload to $_" + "testpac\ succeeded"
        Write-Host $copyMsg   
    }
    Catch{
        $copyMsg = "Upload to $_" + "testpac\ failed!"
        Write-Host $copyMsg    
    }
    
}
}

function Backup-Pac {

<#
.SYNOPSIS
Backup production pac file

.DESCRIPTION
Backup pac file to the backup directory
on the production servers found in the pacserver.txt file

.PARAMETER fileName
Name of the pac file to be backed up

.PARAMETER filePath
Path of the pac file to be backed up

.EXAMPLE
Backup-Pac -fileName somefile.pac -filePath c:\file\path 
#>

Param(
    [Parameter(Mandatory=$true)]
    [string]$fileName,
    [Parameter(Mandatory=$true)]
    [string]$filePath
)

$pacServers = Get-Content C:\windows\system32\WindowsPowerShell\v1.0\Modules\pac_file\pacservers.txt
$today = get-date -Format yyyyMMdd
$noEXT = [IO.Path]::GetFileNameWithoutExtension($fileName)

foreach($_ in $pacServers){
    Try{
        $backup = "$_" +"backup\$noEXT.$today.pac"
        Copy-Item -Path $filePath\$fileName -Destination $backup
        Write-Host "Backup to $backup succeeded"
    }
    Catch{
        Write-Host "Backup to $backup failed!"
    }
    
}
}

function Publish-ProdPac {

<#
.SYNOPSIS
Upload pac file to production directories

.DESCRIPTION
Upload pac file to the production directories of servers
found in the pacserver.txt file

.PARAMETER filePath
Path of the pac file to be uploaded

.EXAMPLE
Publish-ProdPac -filePath c:\file\path\someFile.pac 
#>

Param(
    [Parameter(Mandatory=$true)]
    [string]$filePath
)

$pacServers = Get-Content C:\windows\system32\WindowsPowerShell\v1.0\Modules\pac_file\pacservers.txt
do{
    $overWrite = Read-Host -Prompt "`nDo you really want to overwrite this production pac file? Press y to continue or ctl-c to exit"
}
while("y" -notcontains $overWrite)

foreach($_ in $pacServers){
    Try{
        Copy-Item -Path $filePath -Destination $_
        $copyMsg = "Upload to $_" + " succeeded"
        Write-Host $copyMsg   
    }
    Catch{
        $copyMsg = "Upload to $_" + " failed!"
        Write-Host $copyMsg    
    }
    
}
}

function Remove-Pac {

<#
.SYNOPSIS
Delete specified pac file

.DESCRIPTION
Delete specified pac file from all servers found in
the pacservers.txt file

.PARAMETER fileName
Name of the pac file to be deleted

.PARAMETER pacType
Production (p) Backup (b) or Test (t)

.EXAMPLE
Remove-Pac -fileName someName.pac
#>

Param(
    [Parameter(Mandatory=$true)]
    [string]$fileName,
    [Parameter(Mandatory=$false)]
    [string]$pacType
)

$pacServers = Get-Content C:\windows\system32\WindowsPowerShell\v1.0\Modules\pac_file\pacservers.txt

do{
    $pacType = Read-Host -Prompt "`nIs the file you want to delete in the Production (p), Backup (b) or Test (t) directories?"
}
while ("p","b","t" -notcontains $pacType )    

do{
    $overWrite = Read-Host -Prompt "`nDo you really want to delete this pac file? Press y to continue or ctl-c to exit"
}
while("y" -notcontains $overWrite)

foreach($_ in $pacServers){
    Try{
        if ($pacType -eq "p"){
            $filePath = "$_" + "$fileName"
            Remove-Item -Path $filePath
            Write-Host "Removal  of $fileName from  $filePath Suceeded"
        }
            elseif ($pacType -eq "b"){
                $filePath = "$_" + "backup\$fileName"
                Remove-Item -Path $filePath
                Write-Host "Removal  of $fileName from  $filePath Suceeded"
            }
                else{
                    $filePath = "$_" + "testpac\$fileName"
                    Remove-Item -Path $filePath
                    Write-Host "Removal  of $fileName from  $filePath Suceeded"
                }
    }
    Catch{
        Write-Host "Removal of $fileName Failed!"  
    }
    
}
}