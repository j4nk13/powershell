﻿#Script to format a list of emails for import into proofpoint in order to block
#Place a .txt file with the email addresses on separate lines on your desktop (no spaces after the addresses)

$path = "$home\Desktop\"
$infile = Read-Host -Prompt 'What is the filename (including extension) where the email addresses are stored? '
$comment = Read-Host -Prompt 'What comment would you like to enter into Proofpoint to explain these blocks? '

#Set the path of your email list and import the contents
$emails = Get-Content ($path + $infile)

#Add the info required by proofpoint for import and store in a new object
$output = foreach ($line in $emails){
    if ($line.StartsWith("@")) {
        ("`$from," + "match," + $line + "," + $comment)
    }
    elseif ($line -match '\b(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}\b') {
        ("`$ip," + "equal," + $line + "," + $comment)
    }
    else {
        ("`$from," + "equal," + $line + "," + $comment)
    }
}

#Write the object out to a new file
$output | Out-File "$home\Desktop\formatted_blocks.txt" -Encoding ascii

Write-Host "`nComplete! The formatted_blocks.txt file will be located on your desktop"